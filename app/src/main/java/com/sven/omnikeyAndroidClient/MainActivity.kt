package com.sven.omnikeyAndroidClient

import android.app.AlertDialog
import android.content.*
import android.os.*
import android.smartcardio.TerminalFactory
import android.smartcardio.ipc.ICardService
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.sven.omnikeyAndroidClient.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    private var mService: ICardService? = null
    private val terminalFactory: TerminalFactory? = null

    private var messenger: Messenger? = null //used to make an RPC invocation

    private var isBound = false
    private var connection //receives callbacks from bind and unbind invocations
            : ServiceConnection? = null
    private var replyTo: Messenger? = null //invocation replies are processed by this Messenger


    var it = Intent("com.hidglobal.omnikeydemo.OmniKeyDemoActivity.banana")

    inner class MyBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            /*
            StringBuilder().apply {
                append("Action: ${intent.action}\n")
                append("URI: ${intent.toUri(Intent.URI_INTENT_SCHEME)}\n")
                toString().also { log ->
                    //Timber.d(TAG, log)
                    Toast.makeText(context, log, Toast.LENGTH_LONG).show()
                    println(log)
                }
            }
             */

            if ("com.sven.omnikeyAndroidClient.banana1".equals(intent.action, true)) {
                banana1()
            }
        }


    }

    fun banana1() {
        println("banana")
        Toast.makeText(this, "banana", Toast.LENGTH_LONG).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)



        val navHostFragment =
            (supportFragmentManager.findFragmentById(R.id.nav_host_fragment_content_main) as NavHostFragment?)!!
        val navController = navHostFragment.navController

        binding.navView?.let {
            appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.nav_transform, R.id.nav_reflow, R.id.nav_slideshow, R.id.nav_settings
                ),
                binding.drawerLayout
            )
            setupActionBarWithNavController(navController, appBarConfiguration)
            it.setupWithNavController(navController)
        }

        binding.appBarMain.contentMain.bottomNavView?.let {
            appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.nav_transform, R.id.nav_reflow, R.id.nav_slideshow
                )
            )
            setupActionBarWithNavController(navController, appBarConfiguration)
            it.setupWithNavController(navController)
        }


        binding.appBarMain.fab?.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            // if CardReaderManager is installed bind card service from CardReaderManager app
            //  if (PackageManagerQuery().isCardManagerAppInstalled(this)) {

            this.sendBroadcast(it);
            println("testic")


            if (isBound) {
                //Setup the message for invocation
                val message = Message.obtain(null, 1, 0, 0)
                try {
                    //Set the ReplyTo Messenger for processing the invocation response
                    message.replyTo = replyTo

                    //Make the invocation
                    messenger!!.send(message)
                } catch (rme: RemoteException) {
                    //Show an Error Message
                    Toast.makeText(this@MainActivity, "Invocation Failed!!", Toast.LENGTH_LONG)
                        .show()
                }
            } else {
                Toast.makeText(this@MainActivity, "Service is Not Bound!!", Toast.LENGTH_LONG)
                    .show()
            }




        }

        connection = RemoteServiceConnection()
        replyTo = Messenger(IncomingHandler())


    }

    override fun onStart() {
        super.onStart()

        //Bind to the remote service
        val intent = Intent()
        intent.setClassName(
            "com.hidglobal.omnikeydemo.RemoteService",
            "com.hidglobal.omnikeydemo.RemoteService"
        )
        this.bindService(intent, connection!!, BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()

        //Unbind if it is bound to the service
        if (isBound) {
            unbindService(connection!!)
            isBound = false
        }

    }

    inner class RemoteServiceConnection : ServiceConnection {
        override fun onServiceConnected(component: ComponentName, binder: IBinder) {
            this@MainActivity.messenger = Messenger(binder)
            this@MainActivity.isBound = true
        }

        override fun onServiceDisconnected(component: ComponentName) {
            this@MainActivity.messenger = null
            this@MainActivity.isBound = false
        }
    }

    inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            println("*****************************************")
            println("Return successfully received!!!!!!")
            println("*****************************************")
            val what: Int = msg.what
            Toast.makeText(
                this@MainActivity.applicationContext,
                "Remote Service replied-($what)", Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val result = super.onCreateOptionsMenu(menu)
        // Using findViewById because NavigationView exists in different layout files
        // between w600dp and w1240dp
        val navView: NavigationView? = findViewById(R.id.nav_view)
        if (navView == null) {
            // The navigation drawer already has the items including the items in the overflow menu
            // We only inflate the overflow menu if the navigation drawer isn't visible
            menuInflater.inflate(R.menu.overflow, menu)
        }
        return result
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_settings -> {
                val navController = findNavController(R.id.nav_host_fragment_content_main)
                navController.navigate(R.id.nav_settings)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    companion object {
        private const val REQUEST_BIND_BACKEND_SERVICE_PERMISSION = 9000
        private const val noneOfItemsSelectedByDefault = -1
    }

    private fun showReaderAppNotInstalledDialog() {
        AlertDialog.Builder(this)
            .setTitle("install_card_reader_manager_app")
            .setMessage("install_card_reader_manager")
            .setPositiveButton("OK") { _, _ -> finish() }
            .setCancelable(false)
            .show()
    }

}